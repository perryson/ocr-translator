# OCR Translator
Using Tesseract and Google Translate API, I created a simple command line tool that translated images of foreign texts.

# Image(s):
![Foreign Text 1](https://bitbucket.org/perryson/ocr-translator/raw/master/images/ocr2.png)

![Translated Text 1](https://bitbucket.org/perryson/ocr-translator/raw/master/images/translated2.png)

