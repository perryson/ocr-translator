from PIL import Image
import argparse
import pytesseract
from googletrans import Translator
pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"

#Parser
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--image", required = True, help = "Path to the image")
parser.add_argument("-l", "--language", type = str, required = True)
args = vars(parser.parse_args())

#Main
translator = Translator()

im = Image.open(args["image"])


if args["language"] == "korean":
        text = pytesseract.image_to_string(im, lang="kor")
        print ("Extracted:\n " + text + '\n\nTranslated:\n' + translator.translate(text,dest='en').text)

elif args["language"] == "spanish":
        text = pytesseract.image_to_string(im, lang="spa")
        print ("Extracted:\n " + text + '\n\nTranslated:\n' + translator.translate(text,dest='en').text)

elif args["language"] == "japanese":
        text = pytesseract.image_to_string(im, lang="jpn_vert")
        print ("Extracted:\n " + text + '\n\nTranslated:\n' + translator.translate(text,dest='en').text)


